<?php

namespace App\Application\Api\Controllers;

use App\Http\Controllers\Controller;
use App\Domain\Inventory\Interfaces\OrderInterface;

class OrdersController extends Controller
{
    protected $orderInterface;

    public function __construct(OrderInterface $orderInterface)
    {
        $this->orderInterface = $orderInterface;
    }

    /**
     * fetch order details
     * @param $id
     * @return array
     */
    public function show($id)
    {
        return $this->orderInterface->show($id);
    }
}
