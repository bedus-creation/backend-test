<?php

namespace App\Application\Api\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class OrderResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'order_id' =>  $this->orderNumber,
            'order_date' => $this->orderDate->format('Y-m-d'),
            'status' => $this->status,
            'order_details' => OrderDetailResource::collection($this->orderDetail),
            'bill_amount' => number_format($this->orderDetail->sum('line_total'), 2, '.', ''),
            'customer' => new CustomerResource($this->customer)
        ];
    }
}
