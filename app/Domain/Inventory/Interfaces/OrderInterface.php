<?php

namespace App\Domain\Inventory\Interfaces;

interface OrderInterface
{
    /**
     * Fetch a order based on given order id
     *
     * @param mixed $id
     * @return void
     */
    public function show($id);
}
