<?php

namespace App\Application\Api\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class OrderDetailResource extends JsonResource
{
    /**
     * Transform the resource collection into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            "product" => $this->product->productName,
            'product_line' => $this->product->productLine,
            'unit_price' => (float) $this->priceEach,
            'qty' => $this->quantityOrdered,
            'line_total' => number_format($this->lineTotal, '2', '.', '')
        ];
    }
}
