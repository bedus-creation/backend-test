<?php

namespace App\Domain\Inventory\Models;

use Illuminate\Database\Eloquent\Model;

class OrderDetail extends Model
{
    public $keyType = 'string';

    protected $primaryKey = null;

    protected $table = "orderdetails";

    protected $appends = ['line_total'];

    public function product()
    {
        return $this->belongsTo(Product::class, 'productCode');
    }

    public function getLineTotalAttribute()
    {
        return (float) $this->priceEach * $this->quantityOrdered;
    }
}
