<?php

namespace App\Domain\Inventory\Models;

use Illuminate\Database\Eloquent\Model;

use App\Domain\Users\Models\Customer;

class Order extends Model
{
    protected $keyType = 'string';

    protected $primaryKey = 'orderNumber';

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = [
        'orderDate',
    ];


    /**
     * Get the cutomer who order the products
     */
    public function customer()
    {
        return $this->belongsTo(Customer::class, 'customerNumber', 'customerNumber');
    }

    public function orderDetail()
    {
        return $this->hasMany(OrderDetail::class, 'orderNumber', 'orderNumber');
    }
}
