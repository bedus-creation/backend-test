# Backend Tests
### Details
Name: Bedram Tamang  
Email: tmgbedu@gmail.com


### Things done
#### Architectural change:
As I mentioned earlier in purposal, that I truly believe in **Domain Driven Development(DDD)** architecture patterns and I have tried to implements in basic lavel as only few models is used in this code. In this architecture, It has ```application``` and ```domain``` layers, application layers mustly handles http request and returns the appropriate response. No core business logics is present in application layer where as Domain layers should have core business logics. For more details: [https://stitcher.io/blog/laravel-beyond-crud-01-domain-oriented-laravel](https://stitcher.io/blog/laravel-beyond-crud-01-domain-oriented-laravel)

### Application Layers and Domain layer identification
In this application, we only have **api** related routes, so we put all the api related routes in api domain of application layers. Further, Formrequest, response classes can add into the application layer.

In domain layer, I divide domain layer into two domain i.e. **Users** and **Inventory**. All the users (admin, employer, authenticaion) related core logics should be placed here. It may contains models, action, Jobs, Mail etc.

#### Implementation
* **Be Original if possible:** Change the function name ```fetchOrderData``` to ```show``` (Reason: just to stick with Laravel default **CRUD** methods.)
* **Array callable syntax:** I have used array callable syntax to register controller https://twitter.com/taylorotwell/status/1273368490985037830
```php
Route::get('/orders/{id}', [OrdersController::class, 'show']);
```
* **400 Response:** Order Not Found return json 400 response Implementation  
Instead of manually checking for model not exists, handling exception in **App\Exceptions\Handler** class gives clear implementation for 400 response.
```php
public function render($request, Exception $exception)
{
    if ($exception instanceof ModelNotFoundException) {
        return response()->json(['message' => 'Not Found!'], 400);
    }

    return parent::render($request, $exception);
}
```

Or may be we only want to return 400 status when it wants json response.

```php
public function render($request, Exception $exception)
{
    if ($exception instanceof ModelNotFoundException && $request->wantsJson()) {
        return response()->json(['message' => 'Not Found!'], 400);
    }

    return parent::render($request, $exception);
}
```

* **Laravel Resource API:** Laravel resources is one of the powerful feature in laravel to build api in laravel. I have used laravel resource API to build json response. 
* **Repository Patterns:** Repository Pattern is a great way to decouple your code (loose coupling), I have implemented respository patternsto decouple http requests with business logics.
* **Integration/Unit Tests** All the integration tests are passed.



