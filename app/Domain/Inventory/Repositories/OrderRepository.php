<?php

namespace App\Domain\Inventory\Repositories;

use App\Application\Api\Resources\OrderResource;
use App\Domain\Inventory\Interfaces\OrderInterface;
use App\Domain\Inventory\Models\Order;

class OrderRepository implements OrderInterface
{
    protected $model;

    public function __construct(Order $order)
    {
        $this->model = $order;
    }

    public function show($id)
    {
        $order = $this->model->with('customer', 'orderDetail')
            ->findOrFail($id);
        return new OrderResource($order);
    }
}
